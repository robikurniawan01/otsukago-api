var Sequelize = require("sequelize");

const sequelizeOtsukaGo = new Sequelize(process.env.OTSUKAGO_DB_NAME, process.env.OTSUKAGO_DB_USER, process.env.OTSUKAGO_DB_PASS, {
    host: process.env.OTSUKAGO_DB_HOST,
    dialect: process.env.OTSUKAGO_DB_DIALECT,
    port: process.env.OTSUKAGO_DB_PORT,
    define: {
        timestamps: false,
        timezone: "+07:00"
    },
    logging: false,
    timezone: "+07:00",
    operatorsAliases: 0
});

const sequelizeEmployee = new Sequelize(process.env.EMPLOYEE_DB_NAME, process.env.EMPLOYEE_DB_USER, process.env.EMPLOYEE_DB_PASS, {
    host: process.env.EMPLOYEE_DB_HOST,
    dialect: process.env.EMPLOYEE_DB_DIALECT,
    define: {
        timestamps: false,
        timezone: "+07:00"
    },
    timezone: "+07:00",
    logging: false,
    operatorsAliases: 0,

});


module.exports = {
    sequelizeOtsukaGo,
    sequelizeEmployee
};