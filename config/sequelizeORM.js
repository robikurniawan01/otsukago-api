var fs = require("fs");
var path = require("path");
var Sequelize = require("sequelize");
var config = require("./config");

const Model_aioOtsukaGo = path.join(__dirname, "../models/aio_otsukago");
const Config_aioOtsukaGo = new Sequelize(
    config.otsukago_db.name,
    config.otsukago_db.username,
    config.otsukago_db.password, {
        logging: config.otsukago_db.logging,
        dialect: config.otsukago_db.dialect,
        port: config.otsukago_db.port,
        host: config.otsukago_db.host,
        define: {
            timestamps: false,
            timezone: "+07:00"  
        },
        timezone: "+07:00",
        operatorsAliases: 0
    }
);
const db = {};
let model;

// Otsuka Go
fs.readdirSync(Model_aioOtsukaGo)
    .filter(file => {
        return file.indexOf(".") !== 0 && file.indexOf(".map") === -1;
    })
    .forEach(file => {
        model = require(path.join(Model_aioOtsukaGo, file))(Config_aioOtsukaGo, Sequelize.DataTypes);
        db[model.name] = model;
    });
Object.keys(db).forEach(name => {
    if ("associate" in db[name]) {
        db[name].associate(db);
    }
});

module.exports = db