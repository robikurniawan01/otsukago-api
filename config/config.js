module.exports = {
    otsukago_db: {
        name: process.env.OTSUKAGO_DB_NAME,
        username: process.env.OTSUKAGO_DB_USER,
        password: process.env.OTSUKAGO_DB_PASS,
        host: process.env.OTSUKAGO_DB_HOST,
        dialect: process.env.OTSUKAGO_DB_DIALECT,
        port: process.env.OTSUKAGO_DB_PORT,
        logging: false,

    },
    server: {
        port: process.env.PORT
    },
    security: {
        salt: process.env.OTSUKAGO_SALT

    }
}