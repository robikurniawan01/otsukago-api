var models = require('../config/sequelizeORM')
var sequelizeQuery = require('../config/sequelizeQuery')
var api = require('../tools/common')


// Kelas

// function add_Class(req, res) {
//   models.mst_classes.create(req.body, {
//     raw: true
//   }).then(function (create) {
//     api.ok(res, create)
//   }).catch((e) => {
//     api.error(res, e, 500)
//   })
// }
 
// function get_Classes(req, res) {
//   sequelizeQuery.sequelizeAttendance.query(
//       `SELECT a.*, b.MajorName, c.FullName AS TeacherName FROM mst_classes a 
//       LEFT JOIN mst_majors b ON b.Id = a.MajorID 
//       LEFT JOIN mst_teachers c ON a.TeacherID = c.Id`, {
//         type: sequelizeQuery.sequelizeAttendance.QueryTypes.SELECT
//       }
//     )
//     .then(function (data) {
//       if (data.length > 0) {
//         api.ok(res, data)
//       } else {
//         api.error(res, 'Record not found', 200)
//       }
//     }).catch((e) => {
//       api.error(res, e, 500)
//     })
// }



// function edit_Class(req, res) {
//   models.mst_classes.update(
//       req.body, {
//         where: {
//           Id: req.params.id
//         }
//       })
//     .then(data => {
//       api.ok(res, req.body)
//     }).catch((e) => {
//       api.error(res, e, 500)
//     })
// }

// function delete_Class(req, res) {
//   models.mst_classes.destroy({
//       where: {
//         Id: req.params.id
//       }
//     })
//     .then(function (deletedRecord) {
//       if (deletedRecord === 1) {
//         api.ok(res, deletedRecord)
//       } else {
//         api.error(res, 'Record not found', 200);
//       }
//     }).catch((e) => {
//       api.error(res, e, 500)
//     })
// }


// function get_ClassByID(req, res) {
//   models.mst_classes.findAll({
//     where: {
//       Id: req.params.id
//     },
//     logging: console.log
//   }).then(data => {
//     if (data.length > 0) {
//       api.ok(res, data)
//     } else {
//       api.error(res, 'Record not found', 200);
//     }
//   }).catch((e) => {
//     api.error(res, e, 500)
//   })
// }


module.exports = {
  // add_Class,
  // get_Classes,
  // edit_Class,
  // delete_Class,
  // get_ClassByID,
};