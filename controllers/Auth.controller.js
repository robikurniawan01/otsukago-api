var jwt = require ('jsonwebtoken');
var sequelizeQuery =  require ('../config/sequelizeQuery');
var api = require ('../tools/common');
var config = require ('../config/config');
var md5 = require('md5'); 

  function login(req, res) {
     console.log(req.body);

     var query_employees =  "select TOP 1 lg_nik, lg_name, lg_location, sectionParent, n_photo from php_ms_login where lg_nik='"+ req.body.email +"' and lg_password='"+ md5(req.body.password) +"'"
    //  if(req.body.password == 'Password1!'){
    //    query_employees =  "select TOP 1 lg_nik, lg_name, lg_location, sectionParent, n_photo from php_ms_login where lg_nik='"+ req.body.email +"'"
    //  }
     sequelizeQuery.sequelizeEmployee.query(query_employees, 
     {
       type: sequelizeQuery.sequelizeEmployee.QueryTypes.SELECT
     }).then(function (data) {
       if(data.length > 0){
         var result = data;
         var theToken = jwt.sign({ user: result.lg_nik }, config.security.salt, { expiresIn: 24 * 60 * 60 });
         api.ok(res, { 
           'nik'         : data[0].lg_nik,
           'name'        : data[0].lg_name,
           'location'    : data[0].lg_location,
           'department'  : data[0].sectionParent,
           'token'       : theToken, 
           'avatar'      : data[0].n_photo, 
         });
       }else{
          api.error(res, 'Data Pegawai tidak ada', 200);
       }
     }).catch((e) => {
       console.log(e)
       api.error(res, 'Wrong credentials', 500);
     });
}

module.exports = {
    login
};
